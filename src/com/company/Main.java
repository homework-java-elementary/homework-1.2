package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PointList pointList = null;
        Circle circle;
        double x;
        double y;
        double r;
        int answer;
        do {
            System.out.println("Введите координаты точки:");

            x = getDoubleFromConsole("Введите x:", scanner);

            y = getDoubleFromConsole("Введите y:", scanner);

            pointList = PointList.addPointAtStart(new Point(x, y), pointList);

            System.out.println("Желаете добавить еще (1-да, любое другое число-нет)");
            System.out.print("Ваш выбор:");
            while (!scanner.hasNextInt()) {
                scanner.nextLine();
                System.out.print("Ваш выбор:");
            }
            answer = scanner.nextInt();

        } while (answer == 1);

        x = getDoubleFromConsole("Введите x точки центра окружности:", scanner);

        y = getDoubleFromConsole("Введите y точки центра окружности:", scanner);

        r = getDoubleFromConsole("Введите r окружности:", scanner);

        circle = new Circle(new Point(x, y), r);

        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Список точек:");
        printPointListIntoConsole(pointList);

        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Точки которые принадлежат окружности:");
        PointList pointList1 = pointList;
        while (pointList1 != null) {
            if (circle.containsPoint(pointList1.getPoint())) {
                System.out.println("x = " + pointList1.getPoint().getX() + "," + " y = " + pointList1.getPoint().getY());
            }
            pointList1 = pointList1.nextPoint(pointList1);
        }
    }

    public static double getDoubleFromConsole(String text, Scanner scanner) {
        System.out.print(text);
        while (!scanner.hasNextDouble()) {
            scanner.nextLine();
            System.out.print(text);
        }
        return scanner.nextDouble();
    }

    public static void printPointListIntoConsole(PointList pointList) {
        while (pointList != null) {
            System.out.println("x = " + pointList.getPoint().getX() + "," + " y = " + pointList.getPoint().getY());
            pointList = pointList.nextPoint(pointList);
        }
    }
}
