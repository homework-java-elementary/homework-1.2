package com.company;

public class PointList {
    private Point point;
    private PointList next;

    public PointList(Point point) {
        this.point = point;
        next = null;
    }

    public Point getPoint() {
        return point;
    }

    public static PointList addPointAtStart(Point point, PointList pointList) {
        PointList newPointList = new PointList(point);
        newPointList.next = pointList;
        return newPointList;
    }

    public PointList nextPoint(PointList pointList) {
        return pointList.next;
    }
}
